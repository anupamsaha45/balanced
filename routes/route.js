const Pattern = require("../app/handlers/pattern")
const Login = require("../app/handlers/login")
const UserHandlers = require("../app/handlers/userHandlers")



module.exports = (app) => {
	var payments = [];

	app.route('/get')
		.get(function(req, res) {
			res.status(200).send('Ok!!!')
		})


	app.route('/registration')
		.post(async function(req, res) {
			message = await UserHandlers.createUser(req.body)
			res.send(message)
		})


	app.route('/login')
		.post(async function(req, res) {
			message = await Login.userLogin(req.headers.jwttoken, req.body)
			res.send(message)
					
		})

	app.route('/getAllUsers')
		.get(async function(req, res) {
			
			if(req.headers.jwttoken) {
				message = await UserHandlers.getAllUsers(req.headers.jwttoken)
				res.send(message)
			
			} else 
				res.send("You are not authorized. Pass the jwttoken in header with key name 'jwttoken'")
		})

	app.route('/deleteUser/:userId')
		.get(async function(req, res) {		
				
			if(req.headers.jwttoken) {
				message = await UserHandlers.deleteUser(req.headers.jwttoken, req.params.userId)
				res.send(message)
			
			} else 
				res.send("You are not authorized. Pass the jwttoken in header with key name 'jwttoken'")
		})

	app.route('/balanced')
		.post(async function(req, res) {
			
			if(req.headers.jwttoken) {
				message = await Pattern.parenthesesAreBalanced(req.headers.jwttoken, req.body.pattern)
				res.send(message)
			
			} else 
				res.send("You are not authorized. Pass the jwttoken in header with key name 'jwttoken'")
		})




}

