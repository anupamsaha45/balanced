const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    
    email: String,
    username: String,
    password: String,
    dob:String,
    role: String,
    jwtToken: String,
    attempts: Number,

}, { timestamps: true })
const user = mongoose.model('user', userSchema);

module.exports = user;