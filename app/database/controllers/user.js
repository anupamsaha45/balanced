const User = require('../models/user')


exports.createUser = (data) => {
    return new Promise((resolve, reject) => {
        var _user = new User(data);
        _user.save(function (err, user) {
            if (err) {              
                reject(err);
            }
            resolve(user);
        });
    })
}

exports.getAllUsers = () => {
    return new Promise((resolve, reject) => {
        User.find({}, (err, user) => {
            if (err) {              
                reject(err);
            }
            resolve(user)
        });
    })
}

exports.findUser = (data) => {
    return new Promise((resolve, reject) => {
        User.findOne(data, (err, user) => {
            if (err) {              
                reject(err);
            }
            resolve(user)
        });
    })
}

exports.updateUser = (_user) => {
    return new Promise((resolve, reject) => {
        User.findByIdAndUpdate(_user._id, {
            $set: _user.data
        }, {
            new: true
        }, (err, user) => {
            if (err) {            
                reject(err);
            }
            resolve(user)
        });
    })
}



exports.deleteUser = (userid) => {
    return new Promise((resolve, reject) => {
        User.remove({ '_id': userid }, (err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve(data)
            }
        })
    })
}

