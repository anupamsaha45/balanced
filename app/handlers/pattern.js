const UserCtrl = require("../database/controllers/user")
const Jwt = require('./jwtToken')

exports.parenthesesAreBalanced = async (token, pattern) => {

    try {
        let tokenDetails = await Jwt.verifyToken(token)
        var userId = tokenDetails.token
        userData = await UserCtrl.findUser({ "_id": userId })
        if (userData) {

            userData = await UserCtrl.updateUser({
                "_id": userData._id,
                "data": { "attempts": userData.attempts + 1 }
            })
            var parentheses = "[]{}()",
                stack = [],
                i, character, bracePosition;

            for (i = 0; character = pattern[i]; i++) {
                bracePosition = parentheses.indexOf(character);

                if (bracePosition === -1) {
                    continue;
                }

                if (bracePosition % 2 === 0) {
                    stack.push(bracePosition + 1); // push next expected brace position
                } else {
                    if (stack.length === 0 || stack.pop() !== bracePosition) {
                        return {"username": userData.username, "message": "success", "attempts": userData.attempts}
                    }
                }
            }
            if (stack.length === 0)
                return {"username": userData.username, "message": "success", "attempts": userData.attempts}
            else
                return {"username": userData.username, "message": "Failed", "attempts": userData.attempts}
        } else
            return 'You are not authorized'

    } catch (err) {
        return 'JwtToken is not valid'
    }
}