const UserCtrl = require("../database/controllers/user")
const Jwt = require('./jwtToken')


exports.getAllUsers = async (token) => {
    try {

        let tokenDetails = await Jwt.verifyToken(token)
        var userId = tokenDetails.token

        userData = await UserCtrl.findUser({"_id": userId })
        if (userData) {
            allUsers = await UserCtrl.getAllUsers()
            return allUsers
        } else
            return 'You are not authorized'

    } catch (err) {
        return 'JwtToken is not valid'
    }

}

exports.createUser = async (data) => {
    try {
        userData = await UserCtrl.createUser(data)
        userData = await UserCtrl.updateUser({
                "_id": userData._id,
                "data": { "attempts": 0 }
            })
        return 'Saved successfully'

    } catch (err) {
        return 'An error occured.' + err
    }

}

exports.deleteUser = async (token, paramUserId) => {
    try {

        let tokenDetails = await Jwt.verifyToken(token)
        var userId = tokenDetails.token
        userData = await UserCtrl.findUser({ "_id": userId })
        if (userData) {
            user = await UserCtrl.deleteUser(paramUserId)
            return user
        
        } else
            return 'You are not authorized'

    } catch (err) {
        return 'JwtToken is not valid'
    }
}