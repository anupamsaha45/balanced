const UserCtrl = require("../database/controllers/user")
const Jwt = require('./jwtToken')



exports.userLogin = async (token, data) => {

	userData = await UserCtrl.findUser({"username": data.username})
	
	if(userData && (userData.password == data.password)) {
		let payload = {
	        token: userData._id
	    }
		let token = await Jwt.createToken(payload);
		userData = await UserCtrl.updateUser({
	        _id: userData._id,
	        data: {"jwtToken": token}
	    })
	    return {"token": userData.jwtToken, "message:": "success"}
	
	} else
		return "You are not authorized"
}