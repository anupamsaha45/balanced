## Quick Start

1. Clone the code using the provided Url 
2. cd folderName.
3. run npm install in the terminal.
4. Run nodemon app

---

## API Calls

Use postman to call different APIs provided below.

**1. localhost:1337/registration** using POST method
		
		Pass JSON of below format in the body
		{
			"username": "any user name", 
			"password": "any password",
			"dob": "any dob",
			"role": "any role"
		}
	
	
**2. localhost:1337/login** using POST method
		
		Pass JSON of below format in the body
		{
			"username": "registered username", 
			"password": "registered password"
		}
		
		You will receive success message(if credential are correct) along with "jwtToken". Save it for future purpose.



**3. localhost:1337/getAllUsers** using GET method
		
		Pass jwtToken received above in the headers with key as "jwttoken" and value as received jwTtoken above.



**4. localhost:1337/balanced** using POST method
		
		4.a. Pass jwtToken received above in the headers with key as "jwttoken" and value as received jwTtoken above.
		4.b. Pass JSON of below format in the body
				{
					"pattern": "{[]}"
				}



**5. localhost:1337/deleteUser/userId** using GET method
		
		a. Pass jwtToken received above in the headers with key as "jwttoken" and value as received jwTtoken above.
		b. userId in the param will be any usrId which you have received in point 3.

---
