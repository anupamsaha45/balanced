'use strict'
const config = {
    // jwt secret 
    jwt: {
        jwtSecret: 'assignment',
        tokenExpiry: 365 * 24 *60 * 60,
        errorMsg: 'jwt expired'
    }
}
module.exports = config

