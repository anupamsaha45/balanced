module.exports = {
    // host: "127.0.0.1",
    port: 1337, // change with development port
    mongoUrl: "mongodb+srv://admin123:admin123@cluster0-egowl.mongodb.net/test?retryWrites=true&w=majority", // replace "projectDbName" with a proper db name
    logLevel: "debug", // can be chenged to error, warning, info, verbose or silly
    secret: "superSuperSecret",
};